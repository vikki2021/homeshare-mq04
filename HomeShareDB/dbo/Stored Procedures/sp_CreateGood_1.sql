﻿CREATE PROCEDURE [dbo].[sp_CreateGood]
	
                @Title NVARCHAR,
                @ShortDes NVARCHAR,
                @LongDes  NTEXT ,
                @NrPer INT,
				@Country INT,
				@City NVARCHAR,
				@Street NVARCHAR,
				@Number NVARCHAR,
				@PostCode NVARCHAR,
				@Photo NVARCHAR,
				@AssuranceObligation BIT,
				@IsEnabled BIT,
				@DisabledDate DATE,
				@Latitude NVARCHAR,
				@Longitude NVARCHAR,
				@DateCreation DATE
              
AS
BEGIN
INSERT INTO BienEchange(titre,DescCourte,DescLong,NombrePerson,Pays,Ville,Rue,Numero,CodePostal,Photo,AssuranceObligatoire,isEnabled,DisabledDate,Latitude,Longitude,DateCreation) 
Values(@Title,@ShortDes,@LongDes,@NrPer,@Country,@City,@Street,@Number,@PostCode,@Photo,@AssuranceObligation,@IsEnabled,@DisabledDate,@Latitude,@Longitude,@DateCreation)
	
END