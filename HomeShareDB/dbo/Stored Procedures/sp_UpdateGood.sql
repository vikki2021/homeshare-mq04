﻿CREATE PROCEDURE [dbo].[sp_UpdateGood]
				@IdGood NVARCHAR,
                @Title NVARCHAR,
                @ShortDes NVARCHAR,
                @LongDes  NTEXT ,
                @NrPer INT,
				@Country INT,
				@City NVARCHAR,
				@Street NVARCHAR,
				@Number NVARCHAR,
				@PostCode NVARCHAR,
				@Photo NVARCHAR,
				@AssuranceObligation BIT,
				@IsEnabled BIT,
				@DisabledDate DATE,
				@Latitude NVARCHAR,
				@Longitude NVARCHAR
				
AS
BEGIN
UPDATE BienEchange SET titre=@Title,DescCourte=@ShortDes,DescLong=@LongDes,NombrePerson=@NrPer,Pays=@City,Ville=@City,Rue=@ShortDes,
Numero=@Number,CodePostal=@PostCode,Photo=@Photo,AssuranceObligatoire=@AssuranceObligation,isEnabled=@IsEnabled,
DisabledDate=@DisabledDate,Latitude=@Latitude,Longitude=@Longitude WHERE idBien=@IdGood;
END