﻿CREATE PROCEDURE [dbo].[sp_UserLogin]
		@Email varchar(50),
		@Password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Email,UserPassword FROM [dbo].[Membre] WHERE Email=@Email and UserPassword=@Password
END
