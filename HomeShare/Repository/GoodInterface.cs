﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.Repository
{
    interface GoodInterface
    {
        DataTable getAllGood();
        //here is just an example of the concept, the repo interface must contain all the methods definitions that I'll implement on the DAL class,just as a contract..
    }
}
