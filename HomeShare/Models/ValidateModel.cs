﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeShare.Models
{
    public class ValidateModel
    {
        [Required]
        [Display(Name = "Last Name")]
        [StringLength(maximumLength: 20, MinimumLength = 2, ErrorMessage = "last name must have max length 20 and min length 2")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessage = "last name must have max length 50 and min length 2")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Enter The Proper Email Address!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter the country name.")]
        [Display(Name = "Country")]

        public string Country { get; set; }

        [Required(ErrorMessage = "Enter your phone number.")]
        [Display(Name = "Phone Number")]
       
        public string Phone { get; set; }

        [Required]
        [Display(Name = "User Name")]
        [StringLength(maximumLength: 50, MinimumLength = 2, ErrorMessage = "User name must have max length 50 and min length 2")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", ErrorMessage = "Password Must Contain atleast 8 characters and must have 1 alphabet and 1 number")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string RePassword { get; set; }

        
     
    }
}