﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeShare.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter your e-mail!")]
        [Display(Name = "E-mail:")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "Please enter the password!")]
        [Display(Name = "Password:")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}