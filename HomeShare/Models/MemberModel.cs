﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeShare.Models
{
    public class MemberModel
    {
      
        public string LastName { get; set; }

      
      
        public string FirstName { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        
        public string Phone { get; set; }

        
        public string Login { get; set; }

        public string Password { get; set; }

        
       

        public virtual ICollection<GoodModel> goodModels { get; set; }
    }
}