﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeShare.Models
{
    public class GoodModel

    {
		
		public int IdGood { get; set; }
		[Required]
		public string Title { get; set; }
		[Required]
		public string ShortDes { get; set; }
		[Required]
		public string LongDes { get; set; }
		[Required]
		public int NrPer { get; set; }
		[Required]
		public int Country { get; set; }

		[Required]
		public string City { get; set; }

		[Required]
		public string Street { get; set; }
		[Required]
		public string Number { get; set; }
		[Required]
		public string PostCode { get; set; }
		[Required]
		public string Photo { get; set; }
		[Required]
		public string AssuranceObligation { get; set; }

		public bool IsEnabled { get; set; }

		public DateTime DisabledDate { get; set; }
		[Required]
		public string Latitude { get; set; }
		[Required]
		public string Longitude { get; set; }

		public virtual MemberModel IdMember { get; set; }

		public DateTime DateCreation { get; set; }
	}
}