﻿using System.Web;
using System.Web.Optimization;

namespace HomeShare
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/assets/js").Include(
                      "~/assets/bootstrap/js/bootstrap.js",
                      "~/assets/script.js"));
            bundles.Add(new ScriptBundle("~/assets/owl-carousel/js").Include(
                    "~/assets/owl-carousel/owl.carousel.js"));
            bundles.Add(new ScriptBundle("~/assets/slitslider/js").Include(
                    "~/assets/slitslider/js/modernizr.custom.79639.js",
                    "~/assets/slitslider/js/jquery.ba-cond.min.js",
                     "~/assets/slitslider/js/jquery.slitslider.js"));

            bundles.Add(new StyleBundle("~/assets/css").Include(
                      "~/assets/bootstrap/css/bootstrap.css",
                      "~/assets/style.css"));
            bundles.Add(new StyleBundle("~/assets/owl-carousel/css").Include(
                     "~/assets/owl-carousel/owl.carousel.css",
                     "~/assets/owl-carousel/owl.theme.css"));
            bundles.Add(new StyleBundle("~/assets/slitslider/css").Include(
                     "~/assets/slitslider/css/style.css",
                     "~/assets/slitslider/css/custom.css"));
        }
    }
}
