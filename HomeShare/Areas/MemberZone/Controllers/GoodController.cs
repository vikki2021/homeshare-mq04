﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeShare.DAL;
using HomeShare.Models;

namespace HomeShare.Areas.MemberZone.Controllers
{
    public class GoodController : Controller
    {   //import GoodDAL
        public GoodDAL goodDAL = new GoodDAL();
        // GET: Goods
        public ActionResult Index()
        {
            //Return a list of all the goods from the db
            List<GoodModel> myList = new List<GoodModel>();
            myList = goodDAL.ListGoods().ToList();
            return View(myList);
        }

        //GET:Goods/Create
        public ActionResult Create()
        {
            return View();
        }

        //POST:Goods/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GoodModel good)
        {
            if (ModelState.IsValid)
            { //Insert the form info to db
                goodDAL.CreateGood(good);

                return RedirectToAction("Index");
            }

            //if the model state is not valid
            return View(good);

        }

        // GET:Goods/Details/2
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //check if the worker exist in db
            GoodModel good = goodDAL.ReadOne(id);
            if (good == null)
            {
                return HttpNotFound();
            }
            return View(good);
        }

        // GET:Goods/Delete/3
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //check if the good exist in db
            GoodModel good = goodDAL.ReadOne(id);
            if (good == null)
            {
                return HttpNotFound();
            }
            return View(good);
        }

        //POST:Goods/Delete/3
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            goodDAL.DeleteGood(id);

            return RedirectToAction("Index");

        }

        // GET:Goods/Edit/4
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //check if the good exist in db
            GoodModel dog = goodDAL.ReadOne(id);
            if (dog == null)
            {
                return HttpNotFound();
            }
            return View(dog);
        }

        //POST:Goods/Edit/4
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GoodModel good)
        {
            if (ModelState.IsValid)
            {
                //update db
                goodDAL.UpdateGood(good);
                return RedirectToAction("Index");
            }
            //if the model state is not valid
            return View(good);




        }


    }
}
