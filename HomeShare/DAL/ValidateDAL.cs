﻿using HomeShare.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeShare.DAL
{
    public class ValidateDAL
    {

        string mainconn = ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString;



        public void RegisterUser(ValidateModel vm)
        {
            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_RegisterUser", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;
               
               
     
                sqlcomm.Parameters.AddWithValue("@LastName", vm.LastName);
                sqlcomm.Parameters.AddWithValue("@FirstName", vm.FirstName);
                sqlcomm.Parameters.AddWithValue("@Email", vm.Email);
                sqlcomm.Parameters.AddWithValue("@Country", vm.Country);
                sqlcomm.Parameters.AddWithValue("@Phone", vm.Phone);
                sqlcomm.Parameters.AddWithValue("@Login", vm.Login);
                sqlcomm.Parameters.AddWithValue("@Password", vm.Password);


                //open db connection
                sqlconn.Open();
                //Excute or invoke the CreateDog stored procedure
                sqlcomm.ExecuteNonQuery();
                //Close db connection
          
                
                sqlconn.Close();

                
            }

        }

       
    }
}