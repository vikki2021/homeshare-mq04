﻿using HomeShare.Models;
using HomeShare.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HomeShare.DAL
{
    public class GoodDAL:GoodInterface
    {
      string mainconn = ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString;


        public DataTable getAllGood()
        {
            DataTable goods = new DataTable();
            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlDataAdapter sqldr = new SqlDataAdapter("sp_GetAllGood", sqlconn);
                sqldr.Fill(goods);

            }
            return goods;
        }
        public void CreateGood(GoodModel gd)
        {
            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_CreateGood", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;
                DateTime createAt = DateTime.Now;
                

                //pass in the goodmodel properties
                sqlcomm.Parameters.AddWithValue("@Title", gd.Title);
                sqlcomm.Parameters.AddWithValue("@ShortDes", gd.ShortDes);
                sqlcomm.Parameters.AddWithValue("@LongDes", gd.LongDes);
                sqlcomm.Parameters.AddWithValue("@NrPer", gd.NrPer);
                sqlcomm.Parameters.AddWithValue("@Country", gd.Country);
                sqlcomm.Parameters.AddWithValue("@City", gd.City);
                sqlcomm.Parameters.AddWithValue("@Street", gd.Street);
                sqlcomm.Parameters.AddWithValue("@Number", gd.Number);
                sqlcomm.Parameters.AddWithValue("@PostCode", gd.PostCode);
                sqlcomm.Parameters.AddWithValue("@Photo", gd.Photo);
                sqlcomm.Parameters.AddWithValue("@AssuranceObligation", gd.AssuranceObligation);
                sqlcomm.Parameters.AddWithValue("@IsEnabled", gd.IsEnabled);
                sqlcomm.Parameters.AddWithValue("@DisabledDate", gd.DisabledDate);
                sqlcomm.Parameters.AddWithValue("@Latitude", gd.Latitude);
                sqlcomm.Parameters.AddWithValue("@Longitude", gd.Longitude);
                sqlcomm.Parameters.AddWithValue("@DateCreation", createAt);
        
       



                //open db connection
                sqlconn.Open();
                //Excute or invoke the CreateGood stored procedure
                sqlcomm.ExecuteNonQuery();
                //Close db connection
                sqlconn.Close();



            }
        }

        //Read, Details of a specific good
        public GoodModel ReadOne(int? id)
        {
            GoodModel gm = new GoodModel();

            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_ReadGood", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;

                //pass in the goodmodel properties
                sqlcomm.Parameters.AddWithValue("@IdGood", id);

                //open db connection
                sqlconn.Open();
                SqlDataReader rdr = sqlcomm.ExecuteReader();
                //Excute or invoke the CreateGood stored procedure
                //use while loop

                while (rdr.Read())
                {
                   //gm.IdGood = Convert.ToInt32(rdr["idBien"]);..will modify it later
                    gm.Title = rdr["titre"].ToString();
                    gm.Country = Convert.ToInt32(rdr["Pays"]);
                    gm.City = rdr["Ville"].ToString();
                    gm.DisabledDate =(DateTime) rdr["DisabledDate"];
                }
                //Close db connection
                sqlconn.Close();

            }
            return gm;

        }
        //Update or Edit
        public void UpdateGood(GoodModel gd)
        {
            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_UpdateGood", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;

                //pass in the dogclass model properties
                sqlcomm.Parameters.AddWithValue("@IdGood",gd.IdGood);
                sqlcomm.Parameters.AddWithValue("@Title", gd.Title);
                sqlcomm.Parameters.AddWithValue("@ShortDes", gd.ShortDes);
                sqlcomm.Parameters.AddWithValue("@LongDes", gd.LongDes);
                sqlcomm.Parameters.AddWithValue("@NrPer", gd.NrPer);
                sqlcomm.Parameters.AddWithValue("@Country", gd.Country);
                sqlcomm.Parameters.AddWithValue("@City", gd.City);
                sqlcomm.Parameters.AddWithValue("@Street", gd.Street);
                sqlcomm.Parameters.AddWithValue("@Number", gd.Number);
                sqlcomm.Parameters.AddWithValue("@PostCode", gd.PostCode);
                sqlcomm.Parameters.AddWithValue("@Photo", gd.Photo);
                sqlcomm.Parameters.AddWithValue("@AssuranceObligation", gd.AssuranceObligation);
                sqlcomm.Parameters.AddWithValue("@IsEnabled", gd.IsEnabled);
                sqlcomm.Parameters.AddWithValue("@DisabledDate", gd.DisabledDate);
                sqlcomm.Parameters.AddWithValue("@Latitude", gd.Latitude);
                sqlcomm.Parameters.AddWithValue("@Longitude", gd.Longitude);
            
                //open db connection
                sqlconn.Open();
                //Excute or invoke the CreateDog stored procedure
                sqlcomm.ExecuteNonQuery();
                //Close db connection
                sqlconn.Close();



            }
        }
        //Delete
        public void DeleteGood(int? id)
        {
            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_DeleteGood", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;

                //pass in the goodmoel properties that will be used to locate the good and then delete it.
                sqlcomm.Parameters.AddWithValue("@IdGood", id);

                //open db connection
                sqlconn.Open();
                //Excute or invoke the DeleteGood stored procedure
                sqlcomm.ExecuteNonQuery();
                //Close db connection
                sqlconn.Close();

            }
        }

        //List

        public List<GoodModel> ListGoods()
        {
            List<GoodModel> LGood = new List<GoodModel>();

            using (SqlConnection sqlconn = new SqlConnection(mainconn))
            {
                SqlCommand sqlcomm = new SqlCommand("sp_GetAllGood", sqlconn);
                sqlcomm.CommandType = CommandType.StoredProcedure;

                //open db connection
                sqlconn.Open();
                SqlDataReader rdr = sqlcomm.ExecuteReader();
                //Excute or invoke the CreateDog stored procedure
                //use while loop

                while (rdr.Read())
                {
                    GoodModel gm = new GoodModel();
               
                    gm.Title = rdr["titre"].ToString();
                    gm.Country = Convert.ToInt32(rdr["Pays"]);
                    gm.City = rdr["Ville"].ToString();
                    gm.DisabledDate =(DateTime) rdr["DisabledDate"];
                    gm.DateCreation = (DateTime)rdr["DateCreation"];
                    //populate the Ldog List
                    LGood.Add(gm);


                }

                //Close db connection
                sqlconn.Close();

            }
            return LGood;
        }




    }
}